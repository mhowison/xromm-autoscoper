Author: Andy Loomis
Date: 6/12/2012

This package contains the source code for the xromm autoscoper application. The
autoscoper is designed to perform markerless tracking given biplanar xray video
sequences and a volumetric bone model. It is designed to be used in conjunction
with the XrayProject. More information about these programs can be found online
at the sits below:

www.xromm.org  
www.xromm.org/wiki

The rest of this document is organized into the following sections:

1. Prerequisites
2. Building on Unix
3. Building on Windows

# Prerequisites

The autoscoper software has several dependencies. The first is that you must
have a cuda capable NVIDIA graphics card, and you  must update your graphics
card driver prior to installing the Autoscoper. You must also install the CUDA
compiler and runtime libraries. The latest drivers and CUDA software can be
found at www.nvidia.com. In addition, the autoscoper is built using GTK, so you
must download and install compatible gtk libraries. On unix systems this can
usually be accomplished using your systems default package manager.

# Building on Unix

The autoscoper is built in 2 steps. You can use the top level Makefile to
perform both of those steps. First you must compile the autoscoper libraries in
the libautoscoper directory. It may be necessary to modify the Makefile in that
directory to point to nvcc (nvidia c compiler). Once libautoscoper is built you
can build the autoscoper application itself. You may need to point that Makefile
at the correct locations for GTK and several other libraries.

# Building on Windows

Some limited testing has been done building the autoscoper under windows. The
testing was done on Windows 7 using Visual C++ 2010. The solution files can be
found in the win32/autoscoper subdirectory. It is likely that these files will
need to be modified to get this to build on other systems.

# Building on OS X 10.8

Install XQuartz.  
Install prerequisites with Homebrew:

    brew install gtkglext glew

NOTE: you must modify brew formulae to compile pango with
"--with-included-modules" and gdk-pixbuf with "--with-included-loaders".

Before building, set the search path for pkg-config with:

    export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:/usr/X11/lib/pkgconfig

# Building on OS X 10.7

Same as 10.8, except XQuartz does not need to be installed because X11 is
included with the system.

